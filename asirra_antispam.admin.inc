<?php

/**
 * @file
 * provides page callbacks for configuration page.
 */

/**
 * Admin settings form callback.
 */
function asirra_antispam_settings_form() {
  $form = array();

  $form['asirra_antispam_bigphoto_position'] = array(
    '#type' => 'select',
    '#title' => t('Big Photo Position'),
    '#description' => t("Control where the big version of the photos appear i.e., top, bottom, left, or right."),
    '#default_value' => variable_get('asirra_antispam_bigphoto_position', 'top'),
    '#options' => array(
      'top' => t('Top'),
      'bottom' => t('Bottom'),
      'left' => t('Left'),
      'right' => t('Right'),
    ),
    '#required' => TRUE,
  );
  $cellperrow_options = array();
  for ($i = 1; $i <= 12; $i++) {
    $cellperrow_options[$i] = $i;
  }
  $form['asirra_antispam_cellperrow'] = array(
    '#type' => 'select',
    '#title' => t('Set Cell Per Row'),
    '#description' => t("Control the aspect ratio of the box by changing this constant."),
    '#default_value' => variable_get('asirra_antispam_cellperrow', '6'),
    '#options' => $cellperrow_options,
    '#required' => TRUE,
  );

  $form['asirra_antispam_alert_message'] = array(
    '#title' => t('JS Alert Message After Failure'),
    '#type' => 'textarea',
    '#description' => t('This message will be displayed in Java Script alert dialog boxwhen user fails to select correct answer.'),
    '#default_value' => variable_get('asirra_antispam_alert_message', t('Please correctly identify the cats.')),
  );

  return system_settings_form($form);
}
