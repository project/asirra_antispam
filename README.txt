********************************************************************
                A S I R R A   A N T I S P A M   M O D U L E
********************************************************************
Original Author: Varun Mishra
Current Maintainers: Varun Mishra
Email: varunmishra2006@gmail.com

********************************************************************
DESCRIPTION:

   Asirra is a human interactive proof that asks users to identify photos of 
   cats and dogs. It is based on CAPTCHA module. It is very secure and simple to 
   use. The purpose of this module is to block form submissions by spambot.

********************************************************************

INSTALLATION:

1. Place the entire asirra_antispam directory into sites modules directory
  (eg sites/all/modules).

2. Enable this module by navigating to:

     Administration > Modules

3) This module have dependency on captcha module.

4) Go to admin/config/people/captcha/asirra and configure its settings.

5) Go to admin/config/people/captcha and select "Asirra" as Challenge type for 
   any form.
